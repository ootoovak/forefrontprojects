# Image Attribution

- `projects/birds.jpg` is modified from image [Starling Roost by Tony Armstrong-Sly](https://flic.kr/p/9cwW4j)
- `projects/exo-b.jpg` is modified from image [Silhouette of person holding glass mason jar by Rakicevic Nenad](https://www.pexels.com/photo/silhouette-of-person-holding-glass-mason-jar-1274260://www.pexels.com/photo/silhouette-of-person-holding-glass-mason-jar-1274260/)
- `projects/keeping-the-sites-on.jpg` is modified from image [Chandelier, Santiago de Compostela cathedral by Tanya Hart](https://flic.kr/p/8fi3J5)
- `projects/nas-dock.jpg` is modified from image [Treasure full of seashells by Marco Verch](https://flic.kr/p/27ofFXh)
