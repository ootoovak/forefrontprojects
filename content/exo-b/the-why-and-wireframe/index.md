+++
title = "The why and the wireframe"
description = "A description and wireframe of this burgeoning project."
date = 2019-08-05T08:12:00+12:00
+++

Exo-B is a project I'm just starting up. I've had the name for the project in my head for a while, originally slated for another project in fact, but I've found that it might suit this new project I'm thinking on.

Recently, I've wanted a simple and speedy way to take note that have categories, tags, and mentions. All sortable and searchable. I would like the text entry to be as simple as writing lines to a text file but have it parsable, searchable and sortable by the previously mentioned means. I've recently used tools that have some of this functionality but not all of it or more complex than I need.

Right now all I have is this wireframe of what I imagine the first attempt to look like. You can see the text I would be writing from my keyboard at the top (not actually part of the UI) and then the UI below it. The part in the input box next to the 'SAVE' button contains that same text but contextually parsed with a category at the start, a mention in the middle, and a tag at the end. 

![exo-b-wireframe](exo-b-wireframe.jpg)

_Also examples of what previous entries might look like as well below the input box._

Like many of my projects this is also an opportunity to learn new things. My current plan is to get a simple example going with just plain HTML, CSS ([framework](https://bulma.io/)) and modern vanilla JavaScript. From there I might look at what a [Vue](https://vuejs.org/) and [Yew](https://github.com/yewstack/yew) implementations look like and decide which one I want to carry forward with.

Finally, I think this project will be a prime candidate for trying out a functional pipeline style or programming (a bunch of transformations of data) as well as storing the data in an append-only log kind of way. If I can pull former off then it should be a good project to try store in some of the [distributed append-only log projects](https://github.com/mafintosh/hypercore)!