+++
title = "Exo-B"
description = "Your exo brain for quickly writing what is on your mind so you can reference it later."
date = "2019-07-30"
sort_by = "date"
transparent = true
+++

## Exo-B

The Exo-B project and related code.
