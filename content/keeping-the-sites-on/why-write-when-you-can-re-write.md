+++
title = "Why write when you can re-write!"
description = "Why building your own sites over and over is a bad idea but also maybe an ok idea too."
date = 2019-06-04T12:44:21+12:00

+++

## Like a moth to light

There is a bit of folk knowledge in the developer community that suggests at some point, when you think about starting a blog, you will likely be drawn to also _building your own blogging platform_, which you probably shouldn't, because you are likely procrastinating by [digging a rabbit hole](https://twitter.com/ootoovak/status/932788724542271488). When working professionally on projects for other people you eventually learn that you must acquiesce to time (money) pressures and not follow every new technical whim. But that doesn't apply with personal projects and I think that in part is what can make it so hard to keep momentum up on them.

Well, I managed to half-miss entirely building my own blogging platform the last two times I wanted to get a site up though I still managed to choose bogging platforms pretty much based on the language I was currently most interested in learning rather than a choice based on community activity. As such while not rebuilding a whole platform I did manage to build my own themes, with technologies I had not used, to arguably varying levels of success.

Sine I was [asked before](https://twitter.com/SchuKnight/status/1134926326542901248) for posterity I am going to document a few of the technologies I'm using for the two sites I currently run and a bit more detail on why I made the choices I did.

### Ootoovak.com: the personal blog

I've already written about the [Ootoovak.com](https://ootoovak.com) site, on the site, in the posts ["New site is up!"](https://ootoovak.com/2016/07/05/new-site-is-up/) and ["The Personal and the Project"](https://ootoovak.com/2018/02/02/the-personal-and-the-project/) so I won't retread that ground too much. But as I said I managed to half-miss rebuilding a whole blogging platform and what I meant by that is I didn't rebuild a whole static site generator, instead what I did was find a static site generator written in JavaScript (the language I was most into learning and using that the time) called [HarpJS](http://harpjs.com/) and wrote a whole new theme for it in using [CSS Flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox) because I also wanted to learn about the fancy new CSS feature as well as everything else all at once!

That setup lasted for a little while but then I needed/wanted to rewrite it all because HarpJS didn't seem to have a whole lot of activity around it. So what did next was to find the more-popular [Hexo](https://hexo.io/) and find a community theme that kind of matched what I wanted the site to look like (also written using Flexbox) and then modified it to fit the look I was going for. Thankfully, after that site migration the writing had already done in [Markdown](https://en.wikipedia.org/wiki/Markdown) was pretty easy to bring over to the new site (more on that later).

Of course all that took time away from actually writing any prose _but_ at least I did end up learning about Flexbox amongst a few other things (like [Web Fonts](https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_text/Web_fonts)) which I was actually quite happy with even if I felt a bit guilty for procrastinating writing any posts.

In any case these are some of technologies I eventually settled on for my personal site:

- The static site generator [Hexo](https://hexo.io/).
- Hosting on [Netlify](https://www.netlify.com/).
- Domain name registered on [Name.com](https://www.name.com/).
- Code backed-up on [Gitlab](https://gitlab.com/ootoovak/ootoovak).

### Forefrontprojects.com: the project site

So, you would think after getting that experience with that JavaScript ecosystem, Hexo, and building that Flexbox theme that when it came time to build a new site all those technologies would be at least some reasonable place to start. Well if you thought that you would be right, but also wrong, but mostly right. What _I decided_ to do was what I had already done twice before. Find a static site generator that I hadn't used before, that was written in the language I was currently learning (even if that had little to no impact on any part I would be interacting with), and start with that. This time the language I was learning was Rust and so I chose the newly created Gutenberg (now [Zola](https://www.getzola.org/)) static site generator. Given the project was new and the much smaller Rust community I had to build my own theme from scratch again and this time having recently learnt about CSS Flexbox, what did I do? Well, I went with learning about [CSS Grid Layout](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout)! Yup! Oh, well that is what side projects are all about right? Learning new things and and taking side quests until you don't even really remember why you started. Ha ha. Well, I did end up getting it done in any case, and once again having learnt a bit more about a fancy new CSS feature.

And so now these are some of the technologies I'm using for this project site:

- The static site generator [Zola](https://www.getzola.org/).
- Hosting on [Netlify](https://www.netlify.com/).
- Domain name registered on [Name.com](https://www.name.com/).
- Code backed-up on [Gitlab](https://gitlab.com/ootoovak/forefrontprojects).

## Why would you ever?

So, I may have gone against the common advice even if it was a bit of a middle ground. Besides learning about new ecosystems and CSS features why did I choose to make my own blog sites? Well, for one, I am trying to slowly divest from social media and take control of my own digital content. I don't imagine I'll practically be able to get away any time soon from the big tech giants but having greater control over at least some of my own content is nice. Another reason I chose to take this path is I just like writing my posts in Markdown and most static site generators lend themselves to that pretty well. Having my writing in Markdown to me makes it more portable, and exampled by the transition i made in with my personal site and if I chose to move to another different static site generator or some other format completely, all my posts are very portable as single files. I also happen to like using Git (along with Gitlab and Netlify) as to manage virtually everything from writing templates, prose, and publishing to the web.

## No Analytics

One thing I have kept out of my sites so far is any kind of analytics. I am not currently a business. I'm not trying to drive traffic or "engagement" for the sake of sales. I might be interested to know if I'm getting more views or where people are coming from but I also think as soon as I start to measure that I will consciously or unconsciously start to make choices with that in mind and having it affect me emotionally when numbers go down for example. In some ways my [kitschy design](https://twitter.com/ootoovak/status/1063259518627270656) and not bothering with visitor analytics reminds me a bit of an earlier internet as well which I'm quite happy to sit with for now.

## Next

Well, I'll likely keep tweaking both sites as my needs change. I don't have any plans to move either of them to a different platform right now. I think forefrontprojects.com will continue to be my main focus for now and after I've started to accumulate a bit more content on it I can start thinking a bit more on content organisation and discoverability. Till next time!