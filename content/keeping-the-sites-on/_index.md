+++
title = "Keeping The Sites On"
description = "Notes on the work I do developing this project and my personal site."
date = "2018-11-16"
sort_by = "date"
transparent = true
+++

## Keeping The Sites On

Notes on the work I do developing this project and my personal site.
