+++
title = "The RockPro64 NAS Case Adventure"
description = "Getting set up."
date = 2018-11-16T14:22:00+12:00

+++

## Background

A little while back I bought two [Pine A64+](https://www.pine64.org/devices/single-board-computers/pine-a64/) System on a Chip (SoC) boards to have a play with. They were mostly pretty good and at the time (if I remember right) they were supposed to be the first 64-bit CPUs for a SoC board that was available on the market. I was a fairly early buyer of the board and the Operating System (OS) support for the Pines was a bit lacking at the time but otherwise they mostly did the trick once you got them going. One issue I did have with them though was they were not fully suitable for one of the original use cases I wanted them for which was to be an under Television device. They didn't work as well as originally anticipated due to the chip on the Pine A64+ board being an [Allwinner chip](https://linux-sunxi.org/A64). It only supported hardware accelerated video playback when running the Android TV OS but not while running a Linux distribution because the driver software for the chip not being properly Open Sourced. As such the board didn't work for all use cases I wanted to use my under TV device for which was to be more than just streaming online content.

Even so, I liked the experience enough to be intrigued by when the [Pine Microsystems](https://www.pine64.org/) company (that made the Pine A64+) brought out it's new SoC board called the RockPro64. Not only did the board have more power it used a [Rockchip chip](http://rockchip.wikidot.com/rk3399) instead of an Allwinner one with supposedly better open source support for things like video hardware acceleration on Linux (ironically I am now not actually using the RockPro64 board as a TV attached device but instead am now using an [Nvidia Shield](https://www.nvidia.com/en-us/shield/)). Another use case I was using the boards for was to host Docker containers for things like Time Machine backups and as it turns out a member of the Pine Microsystems community developed and sold a [NAS Case on the Pine online store](https://store.pine64.org/?product=rockpro64-metal-desktopnas-casing) which would suit my purposes very well so I ended up buying it and everything I need to get going.

## The Board And The Case

I already had a couple 3.5 inch external hard drives kicking around so I just needed everything else. Ha! Coming up in computers as an Apple kid I never got into the building of desktop computer towers so this was all new territory for me. Luke, the person from the Pine community that made the NAS Casing, was nice enough to get me [an itemised list of what was needed](https://forum.pine64.org/showthread.php?tid=6129&pid=39889#pid39889) to build the RockPro64 NAS Case and thankfully it was all also available on the Pine Microsystems store. I picked up what I needed and waited for the packages to arive.

## Assembly

As mentioned I had very little experience putting together computers and while I had done some stuff like installing RAM or hard drives into different computers it was never anything too strenuous. As it turns out putting together the NAS Case ended up being not much different except in a few places.

### How do I even heatsink?

So, the NAS Case recommends and provides space for a heatsink plus fan for passive and active cooling respectively. The thing that I was expecting, but knowing had no experience in, was how to apply a thermal pad or thermal paste to a CPU before placing the heatsink on, and I have to say it made me a bit nervous especially considering it came with no instruction. There were a few [helpful links](https://blog.arctic.ac/blog/2015/07/23/thermal-pads-or-thermal-paste/) [and videos](https://www.youtube.com/watch?v=_UeeklKo0Og) for this step (while learning intro stuff like you use one or the other not both!) and after a bit of back and forth in my mind I decided to take the (perceived) higher risk option of using the thermal paste instead of the thermal pad. As I understand it too little paste and there isn't enough heat transfer which could damage the chip under high load with overheating, too much paste and you risk the conductive paste spreading and short circuiting something, damaging the chip or board. During the application process kinda felt like I was doing bomb defusing or something, yikes!

![how-do-i-heatsink](how-do-i-heatsink.jpg)

_Which one do I choose? How do I even use them? Do I cover all three chips or just one? So many questions!_

![heat-sink-installed](heat-sink-installed.jpg)

_Where it sits after I did finally get it installed. Went with the thermal paste on all three chips._

### This sucks! I think.

The second hurdle was trying to get the wiring sorted in such a small enclosed space without the wires hitting the fan blades. This was compounded with trying to get the fan sucking hot air out of the case rather than pulling cool air in ([the optimal choice as I understand it](https://wiki.pine64.org/index.php/NASCase#Step_5._Installing_Extras_.28eMMC.3B_WiFi_BT_module_.2B_SMA_Antennas.3B_80mm_Fan.29)). I successfully got the fan installed with no wires touching, closed the case, powered up the board, and then realised the fan was not sucking air out but rather blowing air in! I'm sure there is some convention or marking to indicate which direction a fan is going to operate but I did not know enough to figure it out the first time but nothing a bit of tenacity couldn't fix.

![them-cables-though-1](them-cables-though-1.jpg)

_It is a tight squeeze with those plugs._

![them-cables-though-2](them-cables-though-2.jpg)

_Those wires are so close to those fan blades! But it works._

![cabling](cabling.jpg)

_They then tuck away along the side to get to the SATA-II borad._

Also, noting that if you are installing Linux there is a software tool to [help regulate the fan's operation](https://forum.pine64.org/showthread.php?tid=6489) once installed.

### Missing some screws... and threads

The case came with screws to attach the hard rives to the casing mounts but a few of them didn't seem to fit quite right when trying to mount one of the hard drives. Luckily, I had some spares but it is something to note.

![hd-screws](hd-screws.jpg)

_There are two hard drives in this case, one mounted above the other._

Another slight issue was that the place to secure the SATA-II Interface Card (that board that connects to the SATA hard drives) with a screw to the case didn't have screw threads in the hole so the screw would not go in. Again it didn't stop putting the thing together (just used a bit of electrical tape) but it is something to be aware of if you are looking to buy the case yourself.

![not-threaded](not-threaded.jpg)

_The electrical tape fix and the paint stripped where I had tried to force the screw to thread._

### Ports and lights

Just a quick mention that there is a "light tube" (a translucent plastic peg) that you put in a hole above the "RST" button that I assume is meant to pipe light from the board's LED power light to outside of the case. I'm not convinced of its effectiveness but I have kept it in anyway. Also, I just wanted to include a few pictures of the ports accessible once the boards are installed.

![case-ports-2](case-ports-2.jpg)

_A nice selection of labeled USB ports._

![case-ports-1](case-ports-1.jpg)

_HDMI port, Ethernet port, power port, and external SATA ports._

### All done!

After all that I got the case screwed together and turned it on and it was all working fine!

![all-put-together](all-put-together.jpg)

_Finally all sorted, just needs the top cover to be screwed on._

## Where We Are At And Up Next

So, this case has been working well for me for a few months now. I got the [community maintained version of Ubuntu](https://github.com/ayufan-rock64/linux-build/releases) working on it and it is happily hosting a few [Docker](https://www.docker.com/) containers that I set up with [Docker Compose](https://docs.docker.com/compose/) and manage using [Portainer](https://www.portainer.io/). Amongst other Docker containers the NAS Case is ticking along happily as a Time Machine back up system now. I don't really push the board that hard right now but I do have plans to add a few more Docker containers to it, likely some of my own making!

![portainer-running-containers](portainer-running-containers.png)

_Now up and running with Docker containers monitored and managed with Portainer._