+++
title = "Contact"
description = "Contact information."
date = 2019-08-19T00:00:00+12:00
+++

If you would like to reach me, this is me across the internet:

- [Ootoovak on Twitter](https://twitter.com/ootoovak)
- [Ootoovak on Gitlab](https://gitlab.com/ootoovak)
- [Ootoovak on LinkedIn](https://www.linkedin.com/in/ootoovak/)
