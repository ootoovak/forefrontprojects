+++
title = "About"
description = "This is the about page."
date = 2017-11-19T00:00:00+12:00
+++

This site is intended to be the place where [I (Samson Ootoovak)](https://ootoovak.com) write posts and keep track of projects I and have been working on. It is a work in progress (as is all the work here) but it is also a start. Trying to retain some of that DIY low-fi quality of the web. :D
