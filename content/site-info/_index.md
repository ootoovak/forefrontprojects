+++
title = "Site Information"
description = "Information about the site and the author."
date = "2019-09-15"
sort_by = "date"
transparent = false
+++

## NAS Dock

A home NAS and Docker host to serve up some practical and experimental projects.
