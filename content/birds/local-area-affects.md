+++
title = "Local Area Affects"
description = "Pulling out a library and making the BoaF project a little less daunting."
date = 2019-02-15T13:12:00+12:00

+++

## A bit much

After working on Circle Boundary for a spell and putting both that and the Birds project down for a bit (work stuff) it was a bit of a mission to pick it back up again. Exact context tends to fall out of my head on projects I haven't looked at for a while and I end up having to load it all back up again if I've been away for a bit. This is especially so if I wrote the project in a language I'm still getting comfortable with like Rust.

This tends to mean my procrastination mode kicks in. I open the code, stare at it, feel it is a bit too hard to get back into again, then distract myself with the Internet.

## Libraries to the rescue

Trying to get back into the project a few times gave me enough context to have it swimming around in my brain again both in the context of solving the "getting started challenge" as well as the actual challenges. Having both challenges process in the background for a while brought me to the conclusion that the main coding problem I had could be pulled out into a library. This would have the benefit of reducing the context I had to keep in my head (once again while still learning Rust!) as well as give me the opportunity to test out a key part of the BoaF swarm mechanics on an individual bird level.

## Local Area Affects

### What is it?

The first library I pulled out for this project was to determine what was the "local area" for a bird as represented by a bitmap (integer based) circle around the bird.

The next step is to figure out what happens to an individual bird when you know which birds are in your local area.

As noted in previous posts I think visualisation is important to understand what is going on. So, I intent for this library to not only make it easy to conceptualise how to approach the code but also conceptualise how the rules applied will affect each individual bird by translating that into visual force vectors on the screen.

Once I came to the idea of this solution I was quite looking forward to it. Part of the appeal of working on the BoaF projects was starting to get back into graphics again (hadn't really done much since university robotics lab projects) but even in it's own small scope at times seemed like I was taking too big of a bite to chew. But, working on an individual bird level, rather than the swarm seemed like a much more manageable piece of work.

### Working it out

I am a visual thinker, so I find skewing out what I want to really helps me. I picked up a piece of paper and worked out the problem.

## Unfinished

Dear readers, this is as far as I got with this post. I think I got back into the code, or stepped away from it completely for a spell, I think a bit of both. In any case I wanted to keep this post. To keep some record of the process. After all this was never intended to be a place just for the finished product but to document the journey or my projects, for myself and others.

