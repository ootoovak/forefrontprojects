+++
title = "Birds of a Feather"
description = "The goals for the Birds of a Feather project."
date = 2018-10-28T11:02:00+12:00
+++

## Vision

Ever since learning about emergent behaviour I have been fascinated with the concept especially how it might apply to artificial life or intelligence. It seems that over and over again you find individual rules can produce complex, unexpected and sometimes even beautiful behaviour.

I also have a soft spot for simulations. Little experiments that help us understand our world a bit better though creating a model of it and the added aspect of it. And visual or auditory simulations can engage parts of our senses that might not otherwise be be engaged in a way that could help give us a deeper understanding and intuition of the data being presented.

So, this project is my continuing experiments into emergent behaviour simulations with an aim to be as much about art as it is about technology.

## Technical Goals

There are a few technical goals I would like to to start with on this project to go along with the overall vision.

I have been learning [Rust](https://www.rust-lang.org/) and this is project is a nice opportunity to keep doing so on something larger and longer living than a tutorial or exercise piece of code.

I also started a project called [Critters](https://ootoovak.com/2015/02/15/critters/) a few years back and found I hit a performance bottleneck (in part I'm sure with the way I was writing it in JavaScript) so I'm curious to see how Rust compiled to [WASM](https://webassembly.org/) performs in a similar simulation.

So, I would like to get this project to a place where it is written in Rust and it displace a visual representation of the simulation both on the Desktop as well as on the web using WASM.

Bonus goal would be to incorporate sound somehow down the track.