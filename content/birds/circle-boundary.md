+++
title = "Circle Boundary"
description = "An extracted library and WASM trial."
date = 2018-11-02T16:52:00+12:00
+++

When setting out to write my [Birds of a Feather](/birds/birds-of-a-feather/) simulation I first started with an Rust application with the intent to chose a graphics library that would allow me to target the desktop as well as browsers via WASM. Part way though I realised I was trying to do two new things at once so I decided to try simplify a bit. Instead of trying to tackle an an graphical application as well as cross compiling I decided to focus on just cross compiling first.

The way I chose to do that was to pull out some functionality I was working on already that didn't have any graphical component and instead just performed some maths that outputted some structured data. And thus Circle Boundary was born.

The concept for the library was to try to reduce the repeated trigonometric calculation of finding out what falls into the "local area" of a bird and instead return a boundary as represented by integers that could be easily offset for each bird. At best this should mean some simple integer comparison to determine what is in a bird's local-area rather quickly eliminating most other birds that need to be considered to affect the current bird you are looking at rather than having to calculate how far *every* other bird is first with floating point trigonometry to determine if it is in the local area.

Of course, once you know what birds are currently in your local-area there will be trigonometric calculations involved to simulate the affect they have on the current bird you are looking at but I'm hoping that the Circle Boundary heuristic will greatly reduce the amount of expensive calculations that need to be made, especially as the number of birds in the simulated world increase.

I have managed to successfully build the Circle Boundary library to at least a pre-release state and I have since released it to both [Crates](https://crates.io/crates/circle_boundary) as well as well as [NPM](https://www.npmjs.com/package/circle_boundary). As of the date of this post I still very much consider this library in a pre-release state but they will evolve with the [Birds of a Feather](/birds/birds-of-a-feather/) project over time.